Obiettivo:

  Rappresentare tutti gli eroagatori anche se vuoti. In particolare, deve essere
  possibile disporre, ad esempio, di 20 pompette, mantenendo l'informazione sul
  fatto che sono disponibili anche se in esse non vi è caricato alcun liquido.

Vincoli:

  1)
    Ogni erogatore è identificato univocamente da un identificativo di tipo stringa.

    Ad esempio con "P1.2" intendiamo la pompa 2 del livello 1. I livelli sono
    numerati dall'alto al basso, per permettere di aggiungere livelli in seguito
    senza staccare i fili di quelli prima.

  2)
    Il nome di un componente può corrispondere a:
      - un erogatore, se il componente è caricato
      - nessun erogatore, se il componente non è caricato.

    NON è ammesso che venga caricato lo stesso componente in erogatori diversi.
    Questo è limitante ma viene fatta questa assunzione per ottenere una maggiore
    semplicità di implementazione

Implementazione:

  Devo implementare l'interfaccia classica delle ComponentList, quindi con getQty e
  getDispenser e getAllComponentsNames. Inoltre, si vuole implementare un interfaccia
  per accedere anche direttamente ai dispenser.

  Inoltre si vorrebbe implementare l'accesso attraverso i magic methods di
  python in modo da avere un'interfaccia migliore per l'accesso (questo presupporrebbe
  anche un adeguamento successivo delle classi sovrastanti). In particolare,
  si vogliono dare 4 sequenze:
   - componentNames: permette di ciclare attraverso i nomi dei componenti
   - dispensersIDs: permette di ciclare tra gli id dei dispensers
   - qtys:
        -bc: permette di ciclare tra le quantità by component
        -bd: permette di ciclare tra le quantità by dispenser
   - dispensers:
        -bc: permette di ciclare tra i dispenser by component
        -bd: permette di ciclare tra le quantità by dispenser


  Dovrò poi prevedere dei metodi per aggiungere/togliere dispensers.

  Sarebbe meglio prevedere una classe dispenser che contenga diverse info, tra cui un'
  istanza di un dispenser reale, ossia di quelli definiti nel modulo hardware, la quantità,
  il nome del componente e la possibilità di vedere se il componente è caricato
  o meno.

  Poi si fa una hashmap con dentro diverse istanze di questa classe dispenser identificati
  dall'id del dispenser. Per accedere ai dispenser dato il nome dei componenti si potrebbe
  fare una sorta di indice, anche se ciò forse non è molto utile, meglio accedere attraverso una
  ricerca iterativa, tanto al massimo si avranno 20-30 dispenser, non ha senso ottimizzare
  a tal punto.

  Poi dovranno essere scatenati gli eventi di notifica delle quantità modificate sia alla
  modifica di una quantità all'interno di un dispenser che all'aggiunta di un dispenser.

  La classe dispenser qua definita deve essere intesa solo come contenitore interno e
  NON deve essere esposta all'esterno.

  Bisogna prevedere delle eccezioni da lanciare quando non c'è un componente o
  un dispenser. Usare delle key error? Ci può stare...


  Metodi:



    - setDispenserComponent(DID,Cname,Cqty):
        imposta il nome del componente e la quantità dello stesso disponibili
        attraverso il dispenser DID. Questo metodo ritorna errore se DID non
        esiste. Per aggiungere nuovi dispenser usare addDispenser
        setDispenserComponent(DID,None,None) scarica il dispenser.
