\select@language {italian}
\select@language {italian}
\contentsline {section}{\numberline {1}Introduzione}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Cosa \IeC {\`e} Cocktailyzer}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Obiettivi}{3}{subsection.1.2}
\contentsline {section}{\numberline {2}Glossario}{5}{section.2}
\contentsline {section}{\numberline {3}Analisi e specifica dei requisiti}{6}{section.3}
\contentsline {subsection}{\numberline {3.1}Requisiti sul sistema di erogazione}{6}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Velocit\IeC {\`a} di erogazione}{6}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}Precisione della miscela}{6}{subsubsection.3.1.2}
\contentsline {subsection}{\numberline {3.2}Requisiti sulle ricette}{8}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Requisiti di usabilit\IeC {\`a}}{8}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Area di gestione}{8}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Requisiti di sicurezza}{9}{subsection.3.5}
\contentsline {subsubsection}{\numberline {3.5.1}Apertura dei serbatoi}{9}{subsubsection.3.5.1}
\contentsline {section}{\numberline {4}Elettronica}{10}{section.4}
\contentsline {subsection}{\numberline {4.1}Scelta del controllore}{10}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Alimentazione}{12}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Circuiti di azionamento degli attuatori}{12}{subsection.4.3}
\contentsline {subsubsection}{\numberline {4.3.1}Elettrovalvole}{14}{subsubsection.4.3.1}
\contentsline {section}{\numberline {5}Software}{17}{section.5}
\contentsline {subsection}{\numberline {5.1}Lato Server}{17}{subsection.5.1}
\contentsline {subsubsection}{\numberline {5.1.1}Modulo \textit {components}}{18}{subsubsection.5.1.1}
