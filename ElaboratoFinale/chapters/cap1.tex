\chapter{Analisi del problema e specifica dei requisiti}

\textit{In questo capitolo si vanno a dettagliare ulteriormente i requisiti accennati nell'introduzione, cercando di estrapolare da essi caratteristiche misurabili sul sistema in modo da poter verificare in seguito se vengono rispettate. Si procede poi all'elencazione precisa di tutte le specifiche di progetto evidenziate.}

\section{Analisi del problema}

\subsection{Classificazione}

Il problema trattato è facilmente classificabile come problema di controllo, in particolare di \emph{controllo digitale}, in quanto si intende utilizzare la tecnologia digitale per gestire tutte le varabili in gioco. Detto ciò risulta evidente il fatto che ci troviamo di fronte ad un problema \emph{real-time}, ossia in cui alcuni task devono essere eseguiti entro delle ben definite scadenze temporali per far si che il loro risultato sia utile.

\medskip 

Dovremo dunque scegliere sistemi di controllo che siano specificamente pensati per applicazioni real time o comunque con potenza di calcolo sufficiente a svolgere tutte le computazioni necessarie entro le scadenze. Ovviamente, grazie alla tecnologia attuale, questo problema nella pratica non sussiste.

\medskip 

Abbiamo poi necessità di ricevere input dall'utente e di presentare ad esso dei risultati. Siamo quindi davanti ad un'applicazione di tipo \emph{interattivo}. Inoltre, ormai, l'utente medio si aspetta da ogni sistema digitale una metodologia d'interazione chiara, semplice ed efficace. Dovremo quindi fornire all'utente un'\emph{interfaccia grafica}. Altra aspettativa ormai comune nella popolazione è quella di poter controllare ogni oggetto direttamente dal proprio smartphone. Il nostro sistema dovrà allora essere distribuito tra l'apparato di controllo e lo smartphone dell'utente, secondo il modello \emph{client-server}. Come accennato in fase di introduzione, i sotto-sistemi comunicanti sono tre. Perciò è necessario gestire due diversi stack client-server, ognuno con le sue peculiarità.

\medskip 

La distribuzione dell'applicazione su diversi sistemi, la molteplicità degli utenti e l'esigenza di avere un'interfaccia reattiva portano con loro la necessità di gestire la \emph{concorrenza} tra più processi che collaborano simultaneamente a fornire all'utente tutti i servizi di cui necessita, andando quindi a generare un complesso ambiente \emph{multi threaded} che va correttamente gestito per evitare problemi difficili da ripetere e diagnosticare.

\medskip

Quindi, nonostante il problema di controllo sia effettivamente semplice, si può notare fin da subito come tutto l'ambiente in cui esso è posto sia articolato e richieda una fase di progettazione attenta ed accurata per poter gestire tale complessità.


\subsection{Problema di controllo}

Quello qui trattato è un classico problema di controllo in retroazione.
\bigskip

Iniziamo facendo una semplificazione. Consideriamo come obiettivo del sistema di controllo quello di riempire il contenitore appoggiato dall'utente con una dose specificata di \emph{un solo} liquido. Questo accorgimento ci permette di rendere più facilmente trattabile il problema, senza diminuire le capacità dello stesso in quanto il comportamento desiderato, ossia il riempimento di un contenitore con una ben definita miscela di liquidi, si può raggiungere semplicemente mettendo in serie una giusta quantità di queste fasi.

\medskip

\begin{figure}[h]
\includegraphics[width=\imgsz]{ctrl_simple}
\centering
\caption{Anello di retroazione semplificato}
\label{fig:ctrl_simple}
\end{figure}

Possiamo identificare nello schema di controllo riportato in figura \ref{fig:ctrl_simple} due principali attori:

\begin{itemize}

\item \emph{Il sistema da controllare (S)}, composto dal bicchiere che deve essere riempito con la dose di liquido richiesta. L'unica variabile di stato presa in esame è la quantità di liquido nel bicchiere, che viene anche selezionata come uscita di S.

\item \emph{Il controllore (C)}, composto dal sistema digitale usato per calcolare in real-time l'entità adeguata dell'azione di controllo su S.

\end{itemize}

Oltre ad essi, possiamo evidenziare altri partecipanti, rappresentati nello schema di figura \ref{fig:ctrl_complex}, che contribuiscono al compimento dell'obiettivo, in particolare:

\begin{itemize}

\item \emph{Trasduttori (T)}, dispositivi di qualsiasi genere destinati a convertire una grandezza fisica in un'altra alterando alcune delle caratteristiche che la identificano. Nel nostro caso abbiamo due tipologie di trasduttori: sensori, che misurano una grandezza fisica e la trasformano in un segnale elettrico in qualche modo correlato e trasduttori, che svolgono il procedimento inverso, ossia convertono un segnale elettrico in un'azione di altra natura sul sistema fisico S.

\item \emph{Convertitore A/D (ADC)}, dispositivo che si occupa di tradurre un segnale analogico (quindi continuo nei tempi e nelle ampiezze) in un segnale digitale (discreto sia nei tempi che nelle ampiezze)

\item \emph{Convertitore D/A (DAC)}, dispositivo che trasforma un segnale digitale in analogico.

\end{itemize}

\begin{figure}[h]
\includegraphics[width=\imgsz]{ctrl_complex}
\centering
\caption{Anello di retroazione completo}
\label{fig:ctrl_complex}
\end{figure}

Nel capitolo 2 si andrà a specificare con precisione quali sistemi reali vanno ad eseguire il compito di ognuno di questi elementi, per ora presentiamo una descrizione astratta.

\bigskip

Vediamo ora il modello matematico di S, sfruttando l'anello semplificato nel caso continuo:
$$\dot{x}=k_1 u$$
$$y=k_2 x$$
ove x è la variabile di stato di S, u è l'ingresso, y è l'uscita e k1 e k2 sono costanti. 

Va inoltre fatta una precisazione: la variabile di ingresso u non può mai essere negativa, in quanto non si dispone di componenti per aspirare liquidi dal bicchiere.

Non sono quindi tollerate sovraelongazioni, in particolare, l'andamento ideale desiderato del sistema retroazionato sarà quello di un sistema lineare del primo ordine che tende asintoticamente al valore target.

\'E evidente che l'unico autovalore del sistema S è 0. Abbiamo quindi un sistema semplicemente stabile, cosa predicibile anche a livello intuitivo.

Possiamo anche ricavare banalmente la funzione di trasferimento:
$$sX(s)=k_1 U(s)$$
$$X(s)=\frac{k_1}{s}U(s)$$
$$Y(s)=k_2X(s)=\frac{k_1k_2}{s}U(s)$$
$$G(s)=\frac{Y(s)}{U(s)}=\frac{k_1k_2}{s}$$

\begin{figure}[h]
\includegraphics[width=\imgszland]{bode_diag}
\centering
\caption{Diagramma di bode di G(s) nel caso $k_1=k_2=1$}
\label{fig:bode_diag}
\end{figure}

La risposta in frequenza del sistema in questione ha fase costante a $-\pi/2$. Dato che S non ha poli instabili, possiamo utilizzare il criterio di Bode per valutare la stabilità del sistema retroazionato. Si ha margine di fase pari a $\pi/2$ qualunque sia il punto in cui il modulo della risposta in frequenza taglia l'asse degli 0dB. Da ciò si deriva che il controllore di cui si necessita è un semplice controllore proporzionale di costante K.

Da tali risultati si può inoltre ricavare che il sistema in anello chiuso si comporterà come un sistema del primo ordine, in particolare:
$$H(s)=\frac{L(s)}{1+L(s)}=\frac{Kk_1k_2/s}{1+Kk_1k_2/s}=\frac{Kk_1k_2}{s+Kk_1k_2}=\frac{1}{\frac{1}{Kk_1k_2}s + 1}$$
Che, come atteso, è un sistema del primo ordine, con un solo polo stabile in $-Kk_1k_2$.

Valutiamo ora l'errore a regime:
$$E(s)/U(s)=\frac{1}{1+L(s)}=\frac{1}{1+Kk_1k_2/s}=\frac{s}{s+Kk_1k_2}$$
Si applica il teorema del valore finale:
$$\lim_{s \to 0}\frac{s}{s+Kk_1k_2} = 0$$
Come atteso, dato che G(s) contiene un integratore. Ottengo quindi che, comunque scelto K, il mio sistema andrà a regime con errore nullo, come desiderato. Questo, ovviamente, a patto di non avere ritardi nel sistema, cosa che porterebbe il margine di fase a diminuire con l'aumentare di K.


Trattandosi di un sistema di controllo \emph{digitale}, dobbiamo considerare anche il tempo di campionamento $T_s$. Per valutare un valore accettabile si potrebbe sfruttare una tra le seguenti regole empiriche:

\begin{itemize}

\item \emph{Metodo della costante di tempo}: $T_s = \frac{1}{10} * T_{min}$, ove $T_{min}$ è la costante di tempo minore del sistema.

\item \emph{Metodo dell'attenuazione}: $T_s=$ inverso della frequenza in cui il diagramma di Bode di L(s) si attenua di 40dB rispetto al guadagno.

\end{itemize}

Tuttavia, le costanti $k_1$ e $k_2$ non sono semplici da stimare. Inoltre, come si potrà vedere nel capitolo 2, la relazione tra l'uscita del controllore e la variabile di controllo U non è lineare per via degli attuatori utilizzati. Si considera quindi pressochè inutile fare delle assunzioni sul tempo di campionamento da queste formule e si preferisce piuttosto tarare il controllore direttamente sul sistema fisico o comunque tramite simulazioni, considerando il tempo di campionamento fornito dai convertitori ADC sufficiente.


\subsection{Valutazione quantitativa dei requisiti}

In questo paragrafo si andranno a riprendere i requisiti indicati in fase di introduzione in modo da meglio interpretarli e trarne delle specifiche misurabili che saranno inserite nella specifica dei requisiti.

\subsubsection{Trasportabilità}

Come già accennato, il sistema finale dovrà essere facilmente trasportabile. Dobbiamo ora definire con chiarezza il concetto di trasportabilità.

\medskip

Diciamo che, per un utente medio, si può considerare trasportabile un sistema che possa essere facilmente contenuto nel bagagliaio di un'automobile, magari un'utilitaria, e che possa essere trasportato facilmente a mano da una singola persona.

\medskip

Essendo fortemente conservativi, possiamo stimare la dimensione del porta bagagli di un'auto di piccole dimensioni attorno ai 200l.
%cita sito http://it.automobiledimension.com/auto-utilitarie.php%
Volendo lasciare spazio per altri oggetti, poniamo un limite massimo all'ingombro del progetto completo a 150l.

Per il trasporto a mano non si dispone di stime precise su quanto peso una persona possa spostare senza problemi per brevi tratti. Cercando di essere sempre il più possibile conservativi, si pone un limite di 5Kg, che secondo l'esperienza soggettiva è considerato accettabile.

\subsubsection{Facilità d'uso}

Pensare ad uno stimatore efficiente per questo tipo di caratteristica non risulta affatto semplice. Possiamo però utilizzare un'approssimazione che comunque risulti facilmente misurabile e che sia discretamente correlato con il parametro da stimare.

\medskip

Si sceglie quindi di valutare il tempo necessario ad un utente completamente estraneo al sistema per essere in grado di far iniziare l'erogazione di un cocktail senza ricevere alcuna spiegazione da altri. Si pone come limite massimo a tale grandezza un tempo di 30 secondi, considerato accettabile.

\subsubsection{Precisione}

La precisione del sistema necessaria a preparare bevande accettabili può essere stimata con maggior accuratezza rispetto a quanto fatto per i requisiti precedenti equiparandola con la precisione di una persona che svolga lo stesso compito.

\medskip

Per stimare la precisione di una persona, si procede effettuando il seguente esperimento: si prendono N operatori (nel nostro caso 2), li si fornisce di alcuni bicchieri e gli si chiede di riempirli esattamente a metà. Si va poi a misurare la quantità realmente versata.

\medskip

Si sceglie di usare la metà per dare un riferimento semplice agli operatori. Inoltre, si preferisce dotarli di bicchieri cilindrici in modo che la metà del volume del bicchiere corrisponda con un livello di acqua pari a metà bicchiere, escluso il fondo.

\bigskip

Questo test valuta l'effettiva capacità di una persona di eseguire nel modo corretto una ricetta senza usare strumenti precisi per il dosaggio, come avviene nei bar. Il nostro scopo è far si che il sistema finale sia in grado di avere una precisione migliore rispetto a quella degli operatori.

\bigskip

Si effettua l'esperimento su due operatori dotati di bicchieri differenti e si riportano i risultati ottenuti:
\begin{itemize}
\item Prima prova, bicchiere da 238ml (necessario versare 119ml):

\bigskip

\begin{center}
	\begin{tabular}{ c | c }
  		Tentativo & Quantità (ml) \\ \hline
  		1 & 108 \\
 		2 & 120 \\
  		3 & 123 \\
  		4 & 116 \\
  		5 & 119 \\
  		6 & 125 \\
  		7 & 122 \\
  		8 & 127 \\
  		9 & 110 \\
  		10 & 122 \\
	\end{tabular}
\end{center}

\bigskip

Otteniamo un errore medio assoluto di 4.8ml, pari al 4.03\% della quantità da versare.

\item Seconda prova, bicchiere da 241ml (necessario versare 120.5ml):

\bigskip

\begin{center}
	\begin{tabular}{ c | c }
  		Tentativo & Quantità (ml) \\ \hline
  		1 & 106 \\
 		2 & 101 \\
  		3 & 112 \\
  		4 & 109 \\
  		5 & 112 \\
  		6 & 110 \\
  		7 & 119 \\
  		8 & 112 \\
  		9 & 122 \\
  		10 & 116 \\
	\end{tabular}
\end{center}

\bigskip

Otteniamo un errore medio assoluto di 8.9ml, pari al 7.39\% della quantità da versare.
\end{itemize}

Calcolando la media tra i due risultati otteniamo un errore di 5.71\%. Si decide di porre come limite massimo per l'errore effettuato dal sistema circa la metà di quello effettuato dall'essere umano. Si fissa quindi a 3\% l'errore massimo ammissibile sulla quantità erogata di un singolo liquido.

\subsubsection{Velocità}

Passiamo ora a valutare la velocità di esecuzione richiesta al sistema per essere 
percepito dall'utente come veloce.

\bigskip

Facendo una ricerca su alcuni siti che propongono ricette di cocktail, possiamo vedere come il tempo medio riportato sia di circa due minuti. Considerando che in tali siti vengono calcolate anche altre fasi della preparazione, momentaneamente non trattate in questo progetto, possiamo stimare come tempo massimo per la realizzazione della miscela circa 45 secondi. Tuttavia, sarebbe ideale avere un sistema sensibilmente più rapido dell'essere umano, perciò fissiamo un limite massimo per la preparazione della miscela a 15 secondi.

\subsubsection{Abbattimento dei costi}

%sito: http://www.mixstory.it/sos-barman/curiosita/stipendio-da-barman-quanto-guadagnano-in-italia.html#axzz4v2l19SnX%

Per valutare questo aspetto, si decide di considerare il guadagno medio a serata di un barista. Facendo qualche ricerca in rete si può fare una media approssimativa che si attesta attorno ai 70-100\euro.

\'E giusto ricordare che l'obiettivo è quello di rendere l'utilizzo del sistema in esame più conveniente rispetto all'assunzione di un barman professionista, tenendo conto anche della possibilità di ammortizzare il costo dell'apparato mediante il riuso.

\medskip

Si considera dunque accettabile una spesa per i soli componenti di un massimo di 200\euro. Considerando un costo del prodotto finito di circa il 50\% superiore al costo dei componenti, si arriva ad una spesa per l'utente di 300\euro, ammortizzabile in circa 5 serate di utilizzo se si considera come stipendio dell'operatore umano 70\euro{} a serata.

\subsubsection{Numero di sostanze miscelabili}

Discutiamo ora quale numero minimo di bevande base considerare. Per fare ciò si è utilizzata semplicemente l'esperienza personale, concludendo che con 15 componenti base correttamente selezionati si riescono a soddisfare le esigenze dell'utente medio. Viene tralasciata l'analisi di quali bevande considerare per brevità.

\subsubsection{Sicurezza}

L'apparato in esame dovrà essere esposto a un gran numero di utenti inesperti e potenzialmente non in pieno possesso delle loro capacità cognitive. Per tale ragione dovranno essere implementate delle misure di sicurezza volte a proteggere l'utente e a difendere il sistema da danni dovuti ad un uso errato dello stesso.

\medskip

Si propongono quindi i seguenti accorgimenti da tenere in considerazione in fase di realizzazione:

\begin{itemize}

\item \emph{Arresto del sistema}: l'apparato deve essere fornito di un sistema di arresto facilmente accessibile dall'utente. In particolare si ritiene opportuno dotarlo di un pulsante di arresto sia grafico all'interno dell'interfaccia dell'app per smartphone che fisico, posizionato nelle vicinanze del sistema di erogazione.

\item \emph{Prevenzione dal versamento di liquido accidentale}: si deve evitare che venga iniziata l'erogazione di una bevanda se non vi è alcun contenitore presente, in modo da non rovinare l'apparato.

\item \emph{Arresto nel caso di bicchiere rimosso prematuramente}: nel caso in cui il bicchiere, o più in generale il contenitore, venga rimosso dall'utente prima del completamento dell'erogazione, il sistema si deve immediatamente arrestare.

\item \emph{Rilevamento della manomissione dei liquidi}: il sistema finale dovrà essere in grado di rilevare la manomissione dei serbatoi contenenti i liquidi che devono essere erogati entrando in stato di allarme e impedendo in tal caso all'utente di richiedere ulteriori bevande per salvaguardare la sua salute.

\end{itemize}

Si dichiara fin da subito che molte di queste feature, in modo particolare l'ultima, saranno lasciate come sviluppi futuri del progetto in quanto al momento ci si è concentrati sulla realizzazione di un apparato funzionante con servizi minimi.

\subsubsection{Adattamento alle richieste del cliente}

Come già accennato, si intende realizzare un sistema il più possibile flessibile ed estensibile, in modo da adattarsi alle richieste degli utenti e di evolvere nel tempo.

\bigskip

Dividiamo la trattazione in due aspetti: lato software e lato hardware.

Sul versante software, appare evidente come risulti necessario pensare ad un'architettura estrememente modulare, riconfigurabile in tempi brevi per adattarsi ai contesti più diversi. Alcune delle caratteristiche richieste sono:

\begin{itemize}

\item \emph{Metodologie di connessione intercambiabili}: il sistema server sarà pensato per connettersi in vario modo con l'utente. In particolare, come si vedrà nel capitolo 2, saranno implementate due tipologie di connessione: la classica TCP/IP e la connessione di tipo Bluetooth, necessaria in contesti in cui l'accesso ad internet non è disponibile.

\item \emph{Possibilità di aggiungere fasi ulteriori oltre alla semplice erogazione di liquidi}: si pensa a mansioni quali aggiunta di ghiaccio, shaking e decorazione.

\item \emph{Riscalabilità delle ricette}: le ricette devono essere facilmente riscalabili in modo da rispondere appieno alle richieste dell'utente.

\item \emph{Predisposizione all'erogazione multipla concorrente}: il sistema dovrà prevedere la futura possibilità di avere più di un erogatore connesso agli stessi serbatoi ed essere quindi in grado di gestire correttamente l'accesso contemporaneo alla risorsa comune.

\end{itemize}


\section{Specifica dei requisiti}

Andiamo ora a elencare in modo compatto tutte le specifiche ricavate nelle sezioni precedenti in modo da avere un riferimento rapido.

\subsubsection{Specifiche quantitative}

\bigskip

\begin{center}
	\begin{tabular}{ l | c }
  		Specifica & Valore \\ \hline
  		Volume massimo dell'apparato & 150 l \\
 		Peso massimo dell'apparato & 5 Kg \\
  		Tempo massimo impegato da un estraneo per apprendere l'utilizzo & 30 s \\
  		Errore massimo tollerato sull'erogazione di un singolo componente & 3 \% \\
  		Tempo massimo per l'erogazione di un cocktail & 15 s \\
  		Costo massimo dei componenti & 200 \euro \\
  		Numero di bevande base & 15 \\
  		Sovraelongazione massima & 0\%
	\end{tabular}
\end{center}

\bigskip

\subsubsection{Specifiche qualitative}

\bigskip

\begin{center}
	\begin{tabular}{ l }
  		Caratteristica voluta\\ \hline
  		Sistema di arresto grafico\\
  		Sistema di arresto fisico\\
  		Prevenzione dal versamento di liquido accidentale\\
  		Arresto nel caso di bicchiere rimosso prematuramente\\
  		Rilevamento della manomissione dei liquidi\\
  		Metodologie di connessione intercambiabili\\
  		Possibilità di aggiungere fasi ulteriori oltre alla semplice erogazione di liquidi\\
  		Riscalabilità delle ricette\\
  		Predisposizione all'erogazione multipla concorrente\\
	\end{tabular}
\end{center}

\bigskip