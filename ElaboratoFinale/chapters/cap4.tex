\chapter{Risultati}

\textit{In questo ultimo capitolo si prendono in esame le caratteristiche dell'apparato una volta completato e testato, confrontandole con quelle evidenziate nella sezione relativa alla specifica dei requisiti. Vengono infine presentati alcuni grafici e alcune misurazioni effettuate sul sistema reale.}

\section{Misurazioni}

Vengono effettuate diverse misurazioni sull'apparato finale, che si riportano nella tabella seguente:

\begin{center}
	\begin{tabular}{ l | c | c }
  		Specifica & Richiesta & Realtà \\ \hline
  		Volume dell'apparato & max 150 l & $\sim$ 20 l\\
 		Peso dell'apparato & max 5 Kg & $\sim$ 2.7 Kg \\
  		Tempo impegato da un estraneo per apprendere l'utilizzo & max 30 s & $\sim$ 15 s\\
  		Errore sull'erogazione di un singolo componente & max 3 \% & $\sim$ 1.5 \%\\
  		Tempo per l'erogazione di un cocktail & max 15 s & $\sim$ 13 s\\
  		Numero di bevande base & 15 & 3 \\
  		Sovraelongazione & 0\% & 0\%
	\end{tabular}
\end{center}
%
Passiamo ora a commentare questi risultati.
\bigskip

Il volume dell'apparato è stato stimato in modo molto approssimativo non avendo ancora realizzato il modulo A. Si è comunque certi che, anche a prodotto finito, il volume sia notevolmente inferiore al limite massimo di 150 l imposto dai requisiti del progetto. Ovviamente, il calcolo del volume non tiene conto dei contenitori in quanto il sistema non richiede alcun serbatoio particolare ed è possibile utilizzare direttamente le bottiglie delle bevande.

\bigskip

Il peso, invece, viene stimato in modo più preciso avendo a disposizione i dati dei vari dispositivi utilizzati. Il modulo A viene approssimato allo stesso peso del modulo B, per quanto riguarda la sola struttura in materiale plastico. La combinazione dei due ha quindi un peso di $212 g * 2 = 424 g$. Bisogna poi tenere conto dei componenti hardware. Si decide di trascurare i dispositivi di controllo e il convertitore ADC in quanto dotati di un peso irrisorio rispetto agli altri componenti. Dai dati relativi alle pompette si ricava che ognuna di esse pesa 110g. Dovendone utilizzare 15, si considera un peso complessivo di 1650g. La cella di carico ha un peso di 30g, mentre i tubi pesano 20g al metro, considerando i 30 metri necessari si arriva ad un peso complessivo di 600g. Il totale risulta quindi essere circa 2.7Kg.

\bigskip

Per quanto riguarda il tempo necessario all'utente a richiedere l'erogazione di un cocktail la prima volta che utilizza il sistema non si hanno dati precisi, dato che non si è ancora eseguito un test pubblico. Essendo però la lista dei cocktail immediatamente mostrata una volta avviata l'applicazione si ritiene che qualunque utente medio sia in grado di richiedere l'esecuzione di una ricetta in pochi secondi.

\bigskip

La precisione, invece, viene stimata semplicemente misurando la quantità reale di liquido erogata. Da una media delle misurazioni effettuate risulta la quantità di errore riportata in tabella. Si nota che tale dato è abbastanza alto e si collega il problema nel fatto che, come accennato nei capitoli precedenti, le pompette non rispondono correttamente al controllo in PWM in quanto valori di duty cycle inferiori a 80\%{} causano l'arresto delle stesse. Con il sistema basato su elettrovalvole, infatti, si riusciva ad ottenere un errore medio inferiore allo 0.1\%, decisamente migliore. Si pensa dunque ad alcune possibili soluzioni al problema:
\begin{itemize}
\item \emph{Riduzione periodo del controllo PWM}: si ritiene che il problema sia legato al fatto che \ardu{} non permette in modo facile di modificare il periodo dell'onda quadra utilizzata per effettuare il controllo PWM. Essendo il periodo di default piuttosto elevato (nell'ordine di alcune decine di KHz), i motori delle pompette non hanno il tempo necessario ad attivarsi nella porzione di periodo in cui l'onda è alta. Per ovviare al problema si potrebbe usare una frequenza di alcuni Hertz per l'onda quadra, in modo da permettere il corretto funzionamento delle pompette, gestendo il tutto da codice scrivendo direttamente sui pin di output digitali.
\item \emph{Rimappamento dell'azione di controllo al range 80-100\%}: la soluzione attualmente adottata per non incorrere nell'arresto delle pompette è di forzare la variabile di controllo a 0.8 quando si trova a valori inferiori. Invece di fare ciò, si potrebbe rimappare la variabile di controllo in modo che 0 corrisponda all'80\%{} e 1 al 100\%{}, attraverso una semplice proporzione oppure funzioni non lineari, se necessario.
\end{itemize}

\bigskip

Il tempo di erogazione del singolo cocktail viene stimato misurando i tempi di diverse sessioni di utilizzo e mediando i risultati trovati. Come atteso, si nota che bevande composte da più liquidi base richiedono maggiori tempi di erogazione rispetto a bevande più semplici. Questo è dovuto al fatto che per ogni componente bisogna raggiungere il set-point, rallentando le pompette nella fase finale, oltre che attendere un piccolo ammontare di tempo per far si che il peso finale si stabilizzi in modo da poter archiviare la quantità precisa erogata per ogni componente base. Questi fattori portano ad avere un tempo medio pericolosamente vicino al massimo accettabile. Per porre rimedio a ciò, si potrebbe rinunciare ad attendere la stabilizzazione del peso finale oppure utilizzare dei tubi di dimensione maggiore per le pompette in modo da aumentarne la portata massima, con la consapevolezza che entrambe le soluzioni porterebbero degli svantaggi il cui impatto sul sistema resta da valutare.

\bigskip

Per il numero di bevande trattate vi è poco da dire, se non che al momento ci si limita a 3 per motivi di tempistica, ma, come ampiamente trattato nei capitoli precedenti, il sistema è predisposto, sia fisicamente che in ambito software, a supportare un numero di componenti più elevato.

\bigskip

Per quanto riguarda la sovraelongazione, essa è chiaramente assente in quanto il sistema retroazionato si comporta come un sistema del primo ordine, fatto confermato dai grafici riportati nella sezione \ref{sec:graphs}.

\section{Analisi dei costi}

Andiamo ora a trattare il costo complessivo del progetto, essendo l'abbattimento dei costi uno dei principali obiettivi del progetto. Vediamo una tabella riassuntiva:

\begin{center}
	\begin{tabular}{ l | c | c}
  		Componente & Costo & Metodo di acquisto \\ \hline
  		\raszero & 10 \euro & The Pi Hut\\
 		\ardu{} Nano (clone) & 1.62 \euro & Importato da AliExpress\\
 		Sceda micro SD (16 GB) & 10 \euro & Amazon\\
 		Cablaggio & 3 \euro & Importato da AliExpress\\
 		Circuito di potenza & 0.3 \euro & Realizzato artiginalmente\\ 
 		Convetitore A/D HX711 & 0.52 \euro & Importato da AliExpress\\ \hline 
 		\textbf{Totale elettronica} & \textbf{25.44 \euro} (\textbf{16.15 \%}) & \\ \hline
 		Modulo A & 5 \euro & Stampato in 3D\\
 		Modulo B & 4.24 \euro & Stampato in 3D\\
 		Tubi di trasporto & 39.60 \euro & Amazon\\ \hline
 		\textbf{Totale struttura} & \textbf{48.84 \euro} (\textbf{30.01 \%}) & \\ \hline
 		Pompette (X 15) & 81.15 \euro & Acquistato lotto su eBay\\
 		Cella di carico & 2.05 \euro & Importato da AliExpress\\ \hline
 		\textbf{Totale sensori e attuatori} & \textbf{83.20 \euro} (\textbf{52.83 \%}) & \\ \hline
 		\textbf{Totale complessivo} & \textbf{157.48 \euro} (\textbf{100 \%})& \\ \hline
 		
  		
	\end{tabular}
\end{center}
%
Il totale di 157.48 \euro{} rispetta il requisito di rimanere al di sotto dei 200 \euro{} per il costo complessivo dell'apparato.
\bigskip

Analizzando rapidamente i dati, appare evidente come il comparto che ha il maggior impatto sul costo finale sia quello dei sensori e degli attuatori, seguito dalla struttura fisica. L'elettronica, invece, risulta l'ambito che ha influito in misura minore sul costo totale. Si può inoltre osservare come la scelta di utilizzare una stampante 3D abbia certamente avuto un impatto negativo sui costi (rispetto alla produzione industriale, strada comunque non percorribile visti i ridotti volumi produttivi), ma tale tale effetto risulta comunque trascurabile rispetto al costo degli altri componenti. Ovviamente, si sceglie di non calcolare nel costo del sistema finale il prezzo della stampante 3D, per i motivi già trattati nei capitoli precedenti. Altro onere che si decide di omettere è quello dovuto all'ingegnerizzazione del sistema, troppo elevato se consideriamo che ha portato alla produzione di un solo prototipo.

\section{Grado di soddisfazione delle specifiche qualitative}

Andiamo ora a riprendere la tabella delle specifiche qualitative prodotta come risultato dell'analisi del problema per esaminarne il grado di soddisfazione.

\begin{center}
	\begin{tabular}{ p{70mm} | p{70mm} }
  		Caratteristica voluta & Grado di soddisfazione\\ \hline
  		Sistema di arresto grafico & Presente all'interno dell'app Android\\ \hline
  		Sistema di arresto fisico & Non ancora implementato\\ \hline
  		Prevenzione dal versamento di liquido accidentale & Non ancora implementata\\ \hline
  		Arresto nel caso di bicchiere rimosso prematuramente & Non ancora implementato\\ \hline
  		Rilevamento della manomissione dei liquidi & Non ancora implementato\\ \hline
  		Metodologie di connessione intercambiabili & Implementato solo lato server. L'app è però già predisposta\\ \hline
  		Possibilità di aggiungere fasi ulteriori oltre alla semplice erogazione di liquidi & Sistema predisposto per questa eventualità\\ \hline
  		Riscalabilità delle ricette & Presente lato server, ricette ancora forzate a 200ml lato client\\ \hline
  		Predisposizione all'erogazione multipla concorrente & Sistema predisposto per questa eventualità\\
	\end{tabular}
\end{center}

Esaminando quanto sopra riportato ci si può rendere conto del fatto che, nonostante si sia raggiunto un buon punto nella realizzazione del sistema, resta ancora molto lavoro da fare soprattutto sotto l'aspetto della sicurezza e dei meccanismi di auto-protezione dell'apparato. Per questo motivo non si considera ancora possibile esporre il dispositivo all'utente finale. Per giungere a tale obiettivo vengono preventivati ancora circa due mesi di lavoro volto ad affinare il progetto e a renderlo sicuro ed affidabile.

\section{Grafici}

\label{sec:graphs}

In questa sezione si vanno a presentare alcuni grafici ricavati dal sistema fisico e si commentano i risultati ottenuti confrontandoli con quelli risultanti dalla simulazione simulink riportata nella sezione \ref{sec:simulink}.

\bigskip



\begin{figure}[h]
\includegraphics[width=\imgszland]{single_component_simulated_0_8_ll}
\centering
\caption{Grafico risultante dalla \emph{simulazione} interna al sistema software}
\label{fig:single_component_simulated_0_8_ll}
\end{figure}

\begin{figure}[h]
\includegraphics[width=\imgszland]{single_component_real_0_8_ll}
\centering
\caption{Grafico dell'erogazione \emph{reale} di un singolo liquido}
\label{fig:single_component_real_0_8_ll}
\end{figure}

Dai grafici ottenuti si può immediatamente notare come il range di variazione della variabile di controllo sia stato limitato all'intervallo [0.8, 1.0] per le considerazioni fatte nella sezione \ref{sec:simulink}. Inoltre, della figura \ref{fig:single_component_real_0_8_ll} si nota che vi è un certo ritardo iniziale prima che l'azione di controllo abbia effetto. Ciò è dovuto al fatto che, al momento dell'inizio dell'erogazione, i tubi di trasporto del liquido erano vuoti e per tale ragione è stato necessario un tempo di circa 1.3 s prima che il liquido iniziasse a fluire nel bicchiere. Il momento in cui la variabile di controllo crolla da 0.8 a 0 corrisponde con il momento del completamento dell'erogazione. In particolare, se la bevanda è composta da un solo liquido base ciò segna il completamento della ricetta e quindi la possibilità per l'utente di ritirare il bicchiere. Se invece il cocktail è composto da più liquidi, tale evento sarà seguito dall'erogazione dei liquidi successivi, come richiesto dalla ricetta. Confrontando questi grafici con quelli ottenuti attraverso simulink, non si notano evidenti discrepanze, se non quelle sopra citate. In particolare, i tempi di erogazione reale e simulata sono paragonabili. Si conclude quindi che il lavoro compiuto in fase di modellizzazione e simulazione sia corretto e che rispecchi in modo fedele la realtà.

