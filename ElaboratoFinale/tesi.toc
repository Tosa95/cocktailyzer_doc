\select@language {italian}
\select@language {italian}
\contentsline {chapter}{Introduzione}{iii}{chapter*.2}
\contentsline {chapter}{Ringraziamenti}{v}{chapter*.3}
\contentsline {chapter}{\numberline {1}Analisi del problema e specifica dei requisiti}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Analisi del problema}{1}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Classificazione}{1}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Problema di controllo}{2}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Valutazione quantitativa dei requisiti}{5}{subsection.1.1.3}
\contentsline {subsubsection}{Trasportabilit\IeC {\`a}}{5}{section*.4}
\contentsline {subsubsection}{Facilit\IeC {\`a} d'uso}{6}{section*.5}
\contentsline {subsubsection}{Precisione}{6}{section*.6}
\contentsline {subsubsection}{Velocit\IeC {\`a}}{7}{section*.7}
\contentsline {subsubsection}{Abbattimento dei costi}{7}{section*.8}
\contentsline {subsubsection}{Numero di sostanze miscelabili}{8}{section*.9}
\contentsline {subsubsection}{Sicurezza}{8}{section*.10}
\contentsline {subsubsection}{Adattamento alle richieste del cliente}{8}{section*.11}
\contentsline {section}{\numberline {1.2}Specifica dei requisiti}{9}{section.1.2}
\contentsline {subsubsection}{Specifiche quantitative}{9}{section*.12}
\contentsline {subsubsection}{Specifiche qualitative}{10}{section*.13}
\contentsline {chapter}{\numberline {2}Progettazione e realizzazione fisica}{11}{chapter.2}
\contentsline {section}{\numberline {2.1}Le possibili alternative}{11}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Struttura fisica del sistema}{11}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Sensori}{12}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Attuatori}{14}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}Dispositivi di conversione analogico digitale}{16}{subsection.2.1.4}
\contentsline {subsection}{\numberline {2.1.5}Controllori}{16}{subsection.2.1.5}
\contentsline {subsubsection}{Arduino}{18}{section*.14}
\contentsline {subsubsection}{Raspberry Pi}{18}{section*.15}
\contentsline {subsubsection}{Microchip PIC}{19}{section*.16}
\contentsline {subsection}{\numberline {2.1.6}Software}{20}{subsection.2.1.6}
\contentsline {section}{\numberline {2.2}L'architettura definitiva}{20}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Sensori}{20}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Attuatori}{21}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Controllori}{23}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Struttura fisica del sistema}{25}{subsection.2.2.4}
\contentsline {subsubsection}{Sistema di aggancio del sensore di peso alla superficie di appoggio}{26}{section*.17}
\contentsline {subsubsection}{Superficie di appoggio}{26}{section*.18}
\contentsline {subsubsection}{Corpo inferiore}{27}{section*.19}
\contentsline {subsubsection}{Ugello di erogazione}{27}{section*.20}
\contentsline {subsubsection}{Matrice di aggancio dei tubi di trasporto dei liquidi}{28}{section*.21}
\contentsline {section}{\numberline {2.3}Alimentazione e circuiti di potenza}{30}{section.2.3}
\contentsline {section}{\numberline {2.4}Schema definitivo e valutazione di fattibilit\IeC {\`a}}{30}{section.2.4}
\contentsline {chapter}{\numberline {3}Architettura software}{35}{chapter.3}
\contentsline {section}{\numberline {3.1}Suddivisione del software}{35}{section.3.1}
\contentsline {subsubsection}{Arduino}{35}{section*.22}
\contentsline {subsubsection}{Raspberry Pi}{36}{section*.23}
\contentsline {subsubsection}{Smartphone}{37}{section*.24}
\contentsline {section}{\numberline {3.2}Software lato Raspberry Pi}{37}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Recipes}{38}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Components}{39}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Executors}{41}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Hardware}{43}{subsection.3.2.4}
\contentsline {subsection}{\numberline {3.2.5}Communication}{44}{subsection.3.2.5}
\contentsline {subsection}{\numberline {3.2.6}Json Translators}{46}{subsection.3.2.6}
\contentsline {subsection}{\numberline {3.2.7}Main System Architecture}{48}{subsection.3.2.7}
\contentsline {subsection}{\numberline {3.2.8}Testing}{49}{subsection.3.2.8}
\contentsline {section}{\numberline {3.3}Lato smartphone}{50}{section.3.3}
\contentsline {chapter}{\numberline {4}Risultati}{54}{chapter.4}
\contentsline {section}{\numberline {4.1}Misurazioni}{54}{section.4.1}
\contentsline {section}{\numberline {4.2}Analisi dei costi}{56}{section.4.2}
\contentsline {section}{\numberline {4.3}Grado di soddisfazione delle specifiche qualitative}{57}{section.4.3}
\contentsline {section}{\numberline {4.4}Grafici}{57}{section.4.4}
\contentsline {chapter}{Conclusioni}{60}{chapter*.25}
