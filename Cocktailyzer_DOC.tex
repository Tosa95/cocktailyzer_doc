\documentclass[12pt]{article}

\usepackage[italian]{babel} 
\usepackage[utf8x]{inputenc}
\usepackage{graphicx}
\usepackage{cleveref}
\usepackage{wrapfig}
\usepackage{hyperref}
\usepackage{multicol}

\graphicspath{ {images/} }

\newcommand{\cbegin}{‘‘}
\newcommand{\cend}{’’}
\newcommand{\imgsz}{0.3\textwidth}
\newcommand{\imgszland}{\textwidth}
\newcommand{\ardu}{Arduino}
\newcommand{\raspi}{Raspberry Pi} 
\newcommand{\raszero} {\raspi{} Zero}
\newcommand{\prjname} {Cocktailyzer}
\newcommand{\blue} {Bluetooth}
\newcommand{\modname}[1]{\textit{#1}}

\renewcommand{\emph}[1]{\textbf{\textit{#1}}}

\pagestyle{headings}
\title{\prjname}
\author{
  Tosatto Davide
}




\makeindex

\begin{document} 


\maketitle
\newpage
\tableofcontents
\newpage

\section{Introduzione}

\subsection{Cosa è \prjname}

\prjname{} è un distributore automatico di cocktail. 
Il problema della preparazione di cocktail risulta essere piuttosto sfaccettato e suddivisibile in diversi sotto problemi, fra cui:
\begin{itemize}
\item Miscelazione dei componenti
\item Aggiunta di ghiaccio
\item Shaking
\item Applicazione di decorazioni quali fettine di agrumi etc.
\item Cocktail speciali (ad esempio il mojito) che presentano una preparazione particolare (aggiunta di foglie di menta, nel nostro esempio)
\end{itemize}

\medskip

Benché certamente il prodotto finito dovrà possedere tutte queste possibilità, la versione qui presentata si concentra solo sulla \emph{miscelazione dei componenti}, in quanto questa specifica caratteristica è certamente la base della preparazione di ogni cocktail e rappresenta dunque un buon punto di partenza.

\subsection{Obiettivi}
\label{obj}
L'obiettivo di questa prima versione di \prjname{} è quello di avere un prototipo funzionante in grado di miscelare diverse componenti liquide per preparare alcuni dei cocktail più comuni.

Il target di questo progetto sono i party improvvisati tra amici, in cui si vogliono avere cocktail preparati con dosi precise, ma senza dover pagare un barman o dover perdere tempo ad imparare le ricette. 

In tale contesto, la macchina sarà probabilmente sottoposta a diversi maltrattamenti, che potrebbero danneggiarla. Essendo questa un'eventualità non improbabile, si rende necessario trovare il giusto equilibrio tra costo ed affidabilità, in modo da poter sostituire componenti che inevitabilmente si guasteranno col minor costo possibile, sacrificando eventualmente anche la precisione.

Questo vincolo sui costi dei componenti verrà ripreso in molte delle scelte progettuali, come sarà possibile verificare nei capitoli successivi.

Abbiamo poi la questione dei tempi di erogazione delle bevande. Certamente l'utente si attende tempi di preparazione da parte di una macchina inferiori rispetto a quelli necessari ad un essere umano per eseguire lo stesso compito.

Detti questi obiettivi principali, si rimanda al capitolo di analisi e specifica dei requisiti la completa elencazione dei requisiti del progetto.

\clearpage

\section{Glossario}

In questa sezione andremo a definire in modo univoco e preciso alcuni dei vocaboli più usati e che potrebbero creare ambiguità se non meglio specificati:

\begin{itemize}
\item \emph{Cocktail}: in questo elaborato associamo a cocktail l'idea di una bevanda composta da una miscela di altre bevande, siano esse alcoliche o analcoliche.

\item \emph{Componente (di un cocktail)}: con componente di un cocktail intendiamo una delle bevande di cui esso è composto.

\item \emph{Componente (presente nel sistema)}: con ciò si vuole intendere una delle bevande presenti all'interno dei contenitori e pronte per essere utilizzate nella preparazione di cocktail. Si fa notare che un componente può essere a sua volta composto da più bevande, se necessario.
\end{itemize}

\clearpage
\section{Analisi e specifica dei requisiti}

\subsection{Requisiti sul sistema di erogazione}

\subsubsection{Velocità di erogazione}

Come detto nella sezione \ref{obj}, è desiderabile proporre agli utenti dei tempi di attesa quantomeno inferiori a quelli che si possono ottenere da un operatore umano. Non avendo dati precisi su quanto tempo sia realmente impiegato per preparare un cocktail in media, possiamo provare a fare una stima approssimativa.

Facendo una ricerca su alcuni siti che propongono ricette di cocktail, possiamo vedere come il tempo medio riportato sia di circa due minuti. 

Considerando che in tali siti vengono considerate anche altre fasi della preparazione, momentaneamente non trattate in questo progetto, possiamo stimare come tempo massimo per la preparazione circa 60 secondi.

Questo, però, ci permetterebbe di ottenere prestazioni simili a quelle di un operatore umano. Essendo il nostro obiettivo quello di far percepire l'effettiva differenza in termini di tempo, possiamo stabilire come obiettivo per il tempo medio di preparazione di un cocktail da parte del nostro apparato circa 10 secondi.

\subsubsection{Precisione della miscela}

Con precisione della miscela si intende la capacità del sistema di eseguire le varie ricette commettendo il minimo errore possibile.

Quanto errore possiamo tollerare? Per stimare questo parametro possiamo basarci sull'operato di una persona alle prese con la preparazione di un cocktail.

La prima osservazione è che la persona non usa alcuno strumento per dosare i componenti, se non i propri sensi.

Possiamo eseguire quindi un esperimento per valutare l'errore commesso da un individuo nel dosaggio di una sostanza liquida. Per fare ciò, diamo ad una persona un bicchiere e chiediamogli di riempirlo a metà. Andiamo poi a misurare la quantità di liquido effettivamente versata e la relazioniamo con la reale quantità che è contenuta in mezzo bicchiere.

Non avendo a disposizione un barman, si esegue l'esperimento su persone non addestrate e si diminuisce l'errore risultante di un fattore 2 per tener conto dell'esperienza di un operatore addestrato.

Mostriamo i risultati:

\begin{itemize}
\item Prima prova, bicchiere da 238ml (necessario versare 119ml):

\bigskip

\begin{center}
	\begin{tabular}{ c | c }
  		Tentativo & Quantità (ml) \\ \hline
  		1 & 108 \\
 		2 & 120 \\
  		3 & 123 \\
  		4 & 116 \\
  		5 & 119 \\
  		6 & 125 \\
  		7 & 122 \\
  		8 & 127 \\
  		9 & 110 \\
  		10 & 122 \\
	\end{tabular}
\end{center}

\bigskip

Otteniamo uno scarto quadratico medio di 5.88ml, pari al 4.94\% della quantità da versare.

\item Seconda prova, bicchiere da 241ml (necessario versare 120.5ml):

\bigskip

\begin{center}
	\begin{tabular}{ c | c }
  		Tentativo & Quantità (ml) \\ \hline
  		1 & 106 \\
 		2 & 101 \\
  		3 & 112 \\
  		4 & 109 \\
  		5 & 112 \\
  		6 & 110 \\
  		7 & 119 \\
  		8 & 112 \\
  		9 & 122 \\
  		10 & 116 \\
	\end{tabular}
\end{center}

\bigskip

Otteniamo uno scarto quadratico medio di 10.36ml, pari al 8.60\% della quantità da versare.
\end{itemize}

Mediando i due risultati otteniamo un errore di 6.77\%. Dividendo per 2 in modo da stimare l'effetto dell'addestramento di un barman, fissiamo come tetto massimo dell'errore tollerato il 3.39\%.

Si noti che per facilitare l'operatore abbiamo utilizzato un bicchiere perfettamente cilindrico, cosa che solitamente non si ha nel contesto della preparazione di cocktail.

\subsection{Requisiti sulle ricette}

Considerando il fatto che il sistema è pensato per un'evoluzione futura e per adattarsi a contesti anche molto diversi, si pongono i seguenti requisiti sulle ricette dei cocktail:

\begin{itemize}

\item \emph{Riscalabilità}: le ricette non devono avere quantità fissate, ma devono specificare solo la proporzione dei componenti, per far si che il sistema si possa adattare a recipienti di volume differente.

\item \emph{Suddivisione in fasi}: le ricette devono poter essere suddivise in fasi ordinate, anche di natura differente (ad esempio: mix di componenti liquidi, aggiunta di ghiaccio, shaking, ecc).

\item \emph{Ricette alternative}: deve poter essere possibile inserire per ogni cocktail ricette alternative, eventualmente con un punteggio che permetta al sistema di valutare quale ricetta eseguire in base anche ai componenti disponibili.

\item \emph{Indipendenza dal programma}: le ricette devono poter essere inserite e/o modificate senza apportare modifiche al programma.

\end{itemize}

\subsection{Requisiti di usabilità}

\subsection{Area di gestione}

Deve essere disponibile un'area di gestione in cui gli utenti amministratori del sistema possono modificare le impostazioni dello stesso, come ad esempio modificare/aggiungere ricette, verificare lo stato dei serbatoi ed effettuare un riempimento, controllare lo stato del sistema di pressurizzazione, ecc.

A tale area non devono poter accedere tutti gli utenti. Ci dovranno quindi essere appositi sistemi di autenticazione e protezione per evitare che si possano verificare situazioni disdicevoli.

\subsection{Requisiti di sicurezza}

\subsubsection{Apertura dei serbatoi}

Ogni apertura non autorizzata dei serbatoi deve essere rilevata e il sistema deve essere messo in stato di blocco.

Una volta bloccato il sistema non potrà più erogare cocktail senza previo sblocco da parte di un utente amministratore.

Per quanto riguarda le aperture autorizzate, ossia richieste attraverso l'area di gestione, devono essere memorizzate ed accessibili dalla stessa area per poter attribuire all'utente responsabile eventuali introduzioni nei serbatoi di sostanze non ammesse.

Anche la rimozione dello stato di blocco deve essere memorizzato e collegato all'utente responsabile.

\newpage
\section{Elettronica}

\subsection{Scelta del controllore}

Come spesso ricorre in progetti di questa tipologia, la scelta del dispositivo da usare per il controllo del sistema ricade su \raspi o su \ardu.

\begin{figure}[h]
\includegraphics[width=\imgszland]{uno}
\centering
\caption{\ardu UNO}
\label{fig:ardu_uno}
\end{figure}

\begin{figure}[h]
\includegraphics[width=\imgszland]{rpi0}
\centering
\caption{\raszero}
\label{fig:rpi0}
\end{figure}

In questo caso si è deciso di optare per \raspi , per i seguenti motivi:

\begin{itemize}

\item \emph{Prezzo}: sorprendentemente, esistono versioni di \raspi , ad esempio \raszero , che hanno un costo inferiore a quello di un \ardu .

\item \emph{Sistema operativo completo}: \raspi{} può contare su un sistema operativo completo basato su Linux, mentre \ardu{} non ha tale caratteristica.

\item \emph{Potenza di calcolo}: \raspi{} ha una potenza di calcolo decisamente superiore rispetto a quella di un \ardu .

\item \emph{Connettività}: nelle sue più recenti versioni, \raspi{} è dotata di tutte le periferiche di connessione più comuni, come Bluetooth, WiFi ed Ethernet.

\item \emph{Memoria di massa}: grazie alla possibilità di inserire una scheda micro SD, \raspi{} permette di avere un quantitativo di memoria più che sufficiente per i nostri scopi (salvare le ricette dei cocktail e altri dati di configurazione), mentre \ardu{} non avrebbe avuto memoria a sufficienza e sarebbe comunque stato più complesso modificare i dati senza riprogrammare il dispositivo.

\end{itemize}

Ovviamente sono stati valutati anche i lati negativi di questa scelta:

\begin{itemize}
\item \emph{Consumo}: questo è senza dubbio il principale motivo per cui \ardu{} potrebbe essere una scelta migliore di \raspi{}, ma è stato considerato accettabile sacrificare qualche mA per poter ottenere tutti i vantaggi che \raspi{} offre.

\item \emph{Ridotte correnti di output}: \raspi{} supporta correnti di output sui pin di I/O decisamente ridotte, ossia circa 50mA \emph{in totale}. Questo dovrà quindi essere considerato quando si andranno a progettare i circuiti di controllo delle periferiche utilizzate.
\end{itemize}

\subsection{Alimentazione}

Si è deciso di suddividere l'alimentazione in due parti: 

\begin{itemize}

\item \emph{Alimentazione \raspi}: per alimentare il controllore si utilizza un alimentatore Micro USB standard con corrente massima di uscita di 2A (come richiesto dalle specifiche di \raspi), anche se i reali consumi si attestano su valori molto inferiori.

\item \emph{Alimentazione attuatori}: Per alimentare gli attuatori utilizziamo un alimentatore a 12V, con corrente massima di uscita di 1.2A. Abbiamo essenzialmente due categorie di attuatori:
\begin{itemize}

\item Elettrovalvole: devono essere alimentate con una tensione di 12V, saranno quindi collegate alla linea a 12V uscente dall'alimentatore.

\item Pompette dell'aria: devono essere alimentate a 5V. Per non dover utilizzare un secondo alimentatore, si fa uso di un regolatore di tensione a 5V (L7805) e si vanno poi a collegare le pompette alla linea di uscita di quest'ultimo.

\end{itemize}

\item \emph{Alimentazione sensori}: si utilizzano le linee a 3.3V o 5.0V (secondo le specifiche del singolo sensore) date in output dalla \raspi. 

\end{itemize}

\subsection{Circuiti di azionamento degli attuatori}

L'idea di base era quella di attivare gli attuatori attraverso dei classici transistor di potenza BJT, nel nostro caso i BD140.
Osservando il datasheet, però, si può osservare come il guadagno (Hfe) minimo garantito sia di solo 40. Essendo la corrente di lavoro di alcuni degli attuatori attorno ai 600mA e potendo la \raspi{} fornire un massimo di 16mA l'uno, ma con un limite massimo di 50mA in totale, risulta non percorribile la strada del pilotaggio diretto dei transistor di potenza da parte della \raspi .

Si opta quindi per inserire un ulteriore stadio di amplificazione mediante transistor BJT SMD BC847. Lo schema circuitale utilizzato è il seguente:

\begin{figure}[h]
\includegraphics[width=\imgszland]{circuito_no_values}
\centering
\caption{Circuito di azionamento degli attuatori}
\label{fig:circ_no_val}
\end{figure}

Alcune considerazioni:
\begin{itemize}

\item La resistenza R1 serve unicamente a mantenere a massa T1 quando non attivo. Dato che desideriamo avere un basso consumo, la poniamo ad un valore che ci permetta di avere una bassa corrente quando il transistor viene attivato, senza avere troppa caduta di tensione per via della corrente inversa di base in stato di interdizione. Fissiamo dunque tale resistenza a $10 K\Omega$, in modo da avere una caduta in stato di interdizione di T2 (IN a 0) di 0.1V (la tensione inversa di base del BD140 si attesta attorno a $10\mu A$).

\item La resistenza R3 serve a limitare la corrente in uscita dalla \raspi. Essendo il guadagno in corrente del BC547 superiore a 100, e servendoci una corrente di collettore sufficiente solo a mandare in saturazione T2, quindi al massimo $20mA$, possiamo calcolare la corrente minima di base necessaria, ossia $0.02/100=200\mu A$. Se prendiamo $R3=10K\Omega$ otteniamo una corrente di base su T2 di $(3.3-0.7)/10000=260\mu A$ sufficiente ad azionare T1. Fissiamo quindi anche R3 a $10K\Omega$.

\item Per semplificare la simulazione, rappresentiamo l'utilizzatore (U) come un resistore. Per valutarne il valore andiamo a dividere la sua tensione di funzionamento per la corrente che in esso scorre (misurata).

\end{itemize}

Resta quindi da dimensionare la sola R2, il cui valore dipenderà dalle correnti e tensioni in gioco per lo specifico utilizzatore. Vediamo ora i valori di R2 calcolati per i vari attuatori corredati dalle simulazioni effettuate, utili a verificare i calcoli.

\subsubsection{Elettrovalvole}

\begin{figure}[h]
\includegraphics[width=\imgszland]{circuito_no_values}
\centering
\caption{Circuito di azionamento degli attuatori}
\end{figure}

Per queste abbiamo $V_{cc} = 12V$, $I_{ev} = 350mA$.

Quindi poniamo:

$$U = \frac{12}{0.35} = 34.29 \Omega$$

Desideriamo avere entrambi i transistori in saturazione, per cui:

$$V_{CE.T1} = -0.2V$$
$$V_{BE.T1} = -0.7V$$

$$V_{CE.T2} = 0.2V$$
$$V_{BE.T2} = 0.7V$$

Per mandare T1 in saturazione dovrò avere:

$$I_{B.T1} > \frac{0.35}{40} = 8.75mA$$

Per sicurezza pongo:
$$I_{B.T1} = 10mA$$

Essendo $I_{R1} = 0.7/10000 = 70\mu A$ trascurabile rispetto a $I_{B.T1}$, pongo $I_{R2} = I_{B.T1} = 10mA$.

\medskip

Posso ora calcolare $R2$:

$$R2 = \frac{V_A - 0.2}{I_{R2}} = 1.11K\Omega \simeq 1K\Omega$$

\bigskip

In figura \ref{fig:circ_val_ev} vediamo lo schema finale.

Dai dati riportati in figura \ref{fig:circ_ev_sim1} possiamo verificare come i nostri calcoli fossero corretti con le inevitabili imprecisioni introdotte dalle numerose approssimazioni.

Mentre con quanto riportato in \ref{fig:circ_ev_sim2} osserviamo che il comportamento del circuito con l'ingresso inattivo è effettivamente quello desiderato.

\begin{figure}[h]
\includegraphics[width=\imgszland]{circuito_values_ev}
\centering
\caption{Circuito di azionamento delle elettrovalvole}
\label{fig:circ_val_ev}
\end{figure}

\begin{figure}[h]
\includegraphics[width=\imgszland]{circuito_ev_sim1}
\centering
\caption{Simulazione con $IN = 3.3V$}
\label{fig:circ_ev_sim1}
\end{figure}

\begin{figure}[h]
\includegraphics[width=\imgszland]{circuito_ev_sim2}
\centering
\caption{Simulazione con $IN = 0V$}
\label{fig:circ_ev_sim2}
\end{figure}

\clearpage

\section{Software}

Essendo un'applicazione di tipo client-server, chiaramente il software sarà suddiviso in due parti, secondo il paradigma thin client, come segue:

\begin{itemize}
\item \emph{Lato \raspi}: qui abbiamo l'applicazione server. Per motivi di semplicità d'uso e di semplice ed efficace interfaccia verso qualsiasi software scritto in linguaggio C, si è deciso di utilizzare Python come linguaggio principale per questo lato dell'applicazione. I compiti del server sono:

\begin{itemize}
\item Mantenere informazioni riguardo ai componenti presenti, le loro quantità e su come erogarli (modulo components)
\item Mantenere informazioni circa i cocktail eseguibili e le loro ricette (modulo recipes)
\item Occuparsi di erogare i cocktail una volta richiesti dall'utente (modulo executors)
\item Fornire tutte le informazioni necessarie al lato utente
\item Gestire la connessione \blue
\end{itemize}

\item \emph{Lato smartphone}: qui risiede il lato client dell'applicazione. Ha il solo compito di mostrare le informazioni all'utente e di trasmettere i suoi comandi al lato server.
\end{itemize}

Andiamo ora a specificare più nel dettaglio il funzionamento delle varie parti del software.

\subsection{Lato Server}

Come detto in precedenza, si utilizza il linguaggio Python. Altra nota di interesse è quella della scelta dell'IDE da utilizzare per il progetto. Sono state prese in considerazione diverse alternative ed alla fine è stato selezionato JetBrains PyCharm, principalmente per due motivi:

\begin{itemize}
\item \emph{Esecuzione remota}: PyCharm supporta l'esecuzione remota di codice e test di unità. Questo ha permesso di velocizzare considerevolmente il processo di sviluppo in quanto è stato possibile testare il software direttamente sul sistema (\raspi{}) senza perdere tempo nel trasferimento manuale di file e conseguente esecuzione da terminale.

\item \emph{Interfaccia simile ad IDE già noti}: PyCharm è degli stessi creatori di IntelliJ IDEA che è l'IDE alla base di AndroidStudio, strumento praticamente irrinunciabile quando si tratta di sviluppare applicazioni Android, e quindi già conosciuto
\end{itemize} 

Il sistema è altamente modularizzato ed è stato reso il più possibile generale per permettere un adattamento rapido ad alcuni degli sviluppi futuri immaginati durante la stesura del progetto iniziale.

Vediamo come sono realizzati i vari moduli e come collaborano tra di loro per ottenere il risultato finale.

\subsubsection{Modulo \modname{components}}

In questo package sono contenute tutte quelle classi che hanno a che fare con la gestione dei componenti del sistema.

In particolare, si desidera mantenere qualche informazione basilare riguardo alle proprietà fisiche dei componenti e, in particolare, informazioni sulla quantità residua di ognuno di essi.

\begin{figure}[h]
\includegraphics[width=\imgszland]{components_module}
\centering
\caption{UML delle classi del modulo \modname{components}}
\label{fig:uml_components}
\end{figure}

In figura \ref{fig:uml_components} possiamo vedere un diagramma UML delle classi riassuntivo del modulo.

Come è possibile notare tutta la gerarchia si diparte dalla classe astratta AbstractCompoenentList e fa poi uso di diversi design pattern.

Ogni AbstractComponentList ha i seguenti metodi (parametri omessi):
\begin{itemize}
\item getComponent(): ritorna le informazioni basilari sul componente
\item getQty(): Ritorna la quantità del componente
\item applyDeltaQuantity(): permette di sottrarre una certa quantità di componente
\item getAllComponentsName(): ritorna una lista contenente i nomi di tutti i componenti
\item endJob(): notifica la conclusione di un task (in particolare, come vedremo sotto, per completare una Reservation ed applicare le modifiche)
\end{itemize}

Il problema principale della realizzazione di questo package era di tenere in considerazione la possibilità (futura) di avere più erogatori afferenti allo stesso serbatoio e quindi che consumano lo stesso componente, diventando sotto l'aspetto software dei processi potenzialmente in concorrenza per una risorsa condivisa, ossia i componenti e in particolare la loro quantità.

Si è allora suddiviso il problema in quattro concetti base:

\begin{itemize}
\item \emph{BaseComponentList}: lista che contiene le informazioni basilari su ogni componente, fra cui densità, gradazione alcolica e quantità. L'accesso a questa lista è sempre mediato da altri oggetti in modo da gestire la concorrenza.

\item \emph{Reservation}: funziona in modo simile ad un decorator per una AbstractComponentList. Essenzialmente una Reservation contiene una lista di componenti associati alla loro quantità prenotata. Quando viene chiamato il metodo getQuantity sulla Reservation, essa ritorna la quantità del componente richiesto data dalla lista di cui è decorator, ma diminuita della quantità prenotata per quel componente (se quel componente è prenotato). Non solo, quando si chiama il metodo applyDeltaQty, la Reservation non applica le modifiche alla lista base, ma le memorizza in una struttura interna.
 
La Reservation è pensata come lista da fornire ad un Executor durante la preparazione di un cocktail in modo che, avendo le quantità già prenotate, si abbia la certezza di poter portare a termine la ricetta senza problemi. Per funzionare correttamente, esegue l'override del metodo endJob, che verrà chiamato dall'esecutore a cocktail pronto. Una volta chiamato tale metodo, la Reservation chiama un callback passato al costruttore, questo per permettere alla ReservationPool di gestire l'applicazione delle modifiche gestendo la concorrenza, ma senza creare una dipendenza ciclica tra le due classi. Si è usato in questo caso una sorta di pattern observer, ma con la differenza che si può avere un solo listener e che lo scopo è unicamente quello di disaccoppiare le due classi.

\item \emph{ReservationPool}: anch'essa è un decorator per una AbstractComponentList (solitamente una BaseComponentList) ed è a sua volta "decorata" da oggetti di tipo Reservation e/o Simulation. La reservation pool contiene una lista di Reservations. Quando viene chiamato il metodo getQuantity viene ritornata la quantità fornita dalla lista base, diminuita della somma di tutte le quantità prenotate per il componente richiesto. Il metodo applyDeltaQty non viene implementato in quanto non è consentito applicare una diminuzione di quantità senza passare per una Reservation. Abbiamo dunque la funzione di callback che verrà passata a tutte le Reservation associate e che verrà quindi chiamata al termine di ognuna di esse e che si occuperà di applicare le quantità effettivamente erogate alla lista base.

La ReservationPool contiene un reentrant lock, che viene acquisito nelle seguenti situazioni:

\begin{itemize}
\item Reservation terminata: lock acquisito, applicazione differenze, lock rilasciato

\item Reservation aggiunta: lock acquisito, reservation creata ed aggiunta, lock rilasciato

\item Utente: viene lasciata all'utente della classe la possibilità di acquisire il blocco nel caso in cui sia necessario vietare per un certo lasso di tempo che le quantità esterne varino (ad esempio quando si esegue una Simulation). Si ricorda che comunque è possibile nel frattempo applicare delle differenze alle Reservation, sarà solo il loro metodo endJob ad essere bloccante.
\end{itemize}

Questo lock risolve essenzialmente il problema della concorrenza serializzando le modifiche effettuate alla lista base, ma permettendo comunque a più erogatori di lavorare contemporaneamente sullo stesso componente spostando la sincronizzazione solo a ricetta completata.

Per evitare problemi, si prevede di sovrastimare le quantità in fase di prenotazione in modo da avere le quantità necessarie per l'esecuzione, con la consapevolezza che comunque a lavoro terminato saranno applicate le quantità \emph{effettivamente utilizzate}.

Questo tipo di lista sarà quella a cui avranno accesso anche gli utenti finali per valutare il grado di consumo dei vari componenti.

\item \emph{Simulation}: questo tipo di oggetto nasce dalla necessità di voler gestire ricette molto generiche e quindi dalla possibilità di non avere abbastanza informazioni per sapere se una ricetta sia eseguibile senza eseguire una simulazione. Ogni esecutore specifico per un certo tipo di fase (ad esempio fase di miscela, fase di aggiunta ghiaccio, ecc) avrà un metodo simulate() che prende in ingresso una simulazione e applica le differenze stimate dovute all'esecuzione di quella particolare fase. Così facendo manteniamo i RecipeExecutors indipendenti dalle fasi che devono eseguire (in parole povere, non devono "guardare all'interno" delle fasi per capire se esse sono eseguibili, ma passerà tutto attraverso il concetto di Simulation.

Questa classe, in modo simile a Reservation, ha una lista base e non vi applica le differenze, nemmeno a simulazione terminate, però mantiene al suo interno tutte le quantità prelevate. Una volta eseguita, è possibile utilizzare il metodo check(), che controlla che le quantità che rimarrebbero al termine della ricetta siano maggiori di una quantità minima (fissata per ragioni di incertezza sul contenuto dei contenitori) e dunque che la ricetta sia eseguibile.

Chiaramente la Simulation è pensata per essere eseguita con il lock sulla ReservationPool acquisito. Non solo. Una volta eseguita la Simulation, se essa ha successo e si desidera trasformarla in reservation, il lock garantisce anche che la situazione riguardante le quantità non vari nel frattempo e quindi che una volta aggiunta la Reservation le quantità siano effettivamente prenotate e disponibili. Quindi il lock andrà rilasciato solo se la simulazione fallisce o una volta effettuata la prenotazione.

\end{itemize}

[TODO: FINIRE E RIORDINARE IL TESTO PERCHE' E' UN PUNTO MOLTO DELICATO]

\end{document}



